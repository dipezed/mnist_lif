from dimitri.utils import *

import tensorflow as tf

class RateDecoding:
    def __init__(self):
        pass
    def forward(self, inputs):
        """
        Rate coding chooses the output neuron with the highest
            firing rate, or spike count, as the predicted class
        Params:
        inputs: tensor of shape(time_window, batch, nb_output)

        Return:
        tensor of shape (batch, 1) 
        """
        # shape:(batch, nb_output)

        #sumation = tf.math.reduce_sum(inputs, axis = 0)
        #res = tf.argmax(sumation, axis = -1)

        sumation = tf.math.reduce_max(inputs, axis = 0)
        return sumation 
