from dimitri.encoding.rate_decoding import RateDecoding
from dimitri.utils import *

import tensorflow as tf

def loss_snn(output, y):
    """
    output shape: (batch, nb_output)
    y shape: (batch,)
    """
    log_p_y = tf.nn.log_softmax(output)
    loss_val = tf.reduce_mean(-log_p_y * tf.one_hot(y, depth=output.shape[-1]))
    return loss_val

class Model:
    def __init__(self,
            optimizer = tf.optimizers.Adam(LEARNING_RATE),
            loss = loss_snn,
            output_decoding = RateDecoding()
            ):
        self.layers = []
        self.optimizer = optimizer
        self.loss = loss
        self.output_decoding = output_decoding
    
        # ==================== METRICS =====================
        self.loss_metric = tf.keras.metrics.Mean(name="loss") 
        self.accuracy_metric = tf.metrics.Accuracy(name='accuracy')


    def add(self, layer):
        self.layers.append(layer)

    def trainable(self):
        res = self.layers[0].trainable_variables
        for layer in self.layers[1:]:
            res += layer.trainable_variables
        return res

    def train_step(self, X, y):
        with tf.GradientTape() as tape:
            output = self(X)
            loss = self.loss(output, y)
        grads = tape.gradient(loss, self.trainable())
        if VERBOSE == 0:
            print(grads)
            print(self.trainable())
        self.optimizer.apply_gradients(zip(grads, self.trainable()))
        self.loss_metric.update_state(loss)
        
        predicted_labels = tf.argmax(output, axis=-1)
        self.accuracy_metric.update_state(y, predicted_labels)
        return {"loss":self.loss_metric.result().numpy(),
                "acc": self.accuracy_metric.result().numpy()}

    def train(self, X, y, epoch = EPOCH,
            batch_size = BATCH_SIZE,
            verbose = VERBOSE):
        """
        Train the model
        X: shape(time, all_batch, nb_neuron)
        y: shape(all_batch, nb_output)
        """
        metrics = None
        for e in range(epoch):
            i = 0
            while X.shape[1] >= i + batch_size:
                metrics = self.train_step(
                    X[:, i:i+batch_size],
                    y[i:i+batch_size])
                i+=batch_size
            if e != 0 and e % VERBOSE == 0:
                print(metrics)

    def call(self, inputs):
        """
        inputs: tensor shape(time, batch, nb_neuron)
        """
        time_window = tf.shape(inputs)[0]
        batch = tf.shape(inputs)[1]
        #output_snn = tf.zeros((time_window, batch, self.output_size))
        output_snn = []
        for t in range(time_window):
            X = self.layers[0].forward(inputs[t])
            for layer in self.layers[1:]:
                X = layer.forward(X)
            #output_snn[t] = X
            output_snn.append(X)
        output_tensor_snn = tf.convert_to_tensor(output_snn)
        output = self.output_decoding.forward(output_tensor_snn)
        return output 
        
    def __call__(self, inputs):
        return self.call(inputs)
