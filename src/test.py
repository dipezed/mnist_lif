#!/usr/bin/python3

import os
# ----- Remove Tensorflow Warning --------
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import dimitri.encoding.rate_encoding as rate_encoding
from dimitri.model.model import Model
from dimitri.model.neuron import LIFNeuron

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf



def main():

    inputs = np.random.rand(2, 4)
    X = rate_encoding.rate_encode(inputs,
            is_output_tensor = True)
    neurons1 = LIFNeuron(4, 3, ploting=True)
    model = Model()
    model.add(neurons1)
    output = model(X)
    print("output = ", output.numpy())
    neurons1.plot(["I", "U", "S"])

main()



