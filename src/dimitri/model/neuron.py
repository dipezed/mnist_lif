from dimitri.utils import *
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np

class LIFNeuron(tf.keras.layers.Layer):
    def __init__(self,
            input_size,
            nb_neuron,
            alpha = ALPHA,
            beta = BETA,
            ploting = False,
            is_output = False
            ):
        super(LIFNeuron, self).__init__()
        self.alpha = ALPHA
        self.beta = BETA

        self.input_size = input_size
        self.nb_neuron = nb_neuron
        self.is_output = is_output

        self.w =  tf.random.uniform(
                    (input_size, nb_neuron),
                    minval = MIN_VAL,
                    maxval = MAX_VAL,
                    seed = SEED
                    )
        self.inhibitory = tf.random.uniform(
                (input_size, nb_neuron),
                minval = MIN_VAL,
                maxval = MAX_VAL,
                seed = SEED
                )
        self.inhibitory = tf.where(self.inhibitory < INHIBITORY, True, False)

        self.w = tf.Variable(
                    tf.where(self.inhibitory, -self.w, self.w),
                    trainable = True
                    )

        # ==== PLOT ======
        self.legend = []
        self.start = 1
        self.end = -1
        self.index = ()
        self.ploting = ploting
        # ================
        self.I = 0
        self.S = 0
        self.U = 0
        if self.ploting:
            self.t = [0]
            self.I = [0]
            self.S = [0]
            self.U = [0]
            self.forward = self.forward_plt

    @tf.custom_gradient
    def heaviside(self, U):
        out = tf.where(U > THRESHOLD, 1.0, 0.0)
        def grad(dy):
            return dy * (SURR_GRAD_SCALE * tf.square(1.0 / (tf.abs(U) + 1.0)))
        return out, grad

    def forward_plt(self, X):
        """
        this function will erease `forward` if the self.ploting = true
        X: tensor of shape (batch, input_size)
        """
        self.t.append(self.t[-1] + DT)
        # time: t
        self.I.append(self.alpha * self.I[-1] + X @ self.w)
        self.U.append((self.beta * self.U[-1] + self.I[-1]) * (THRESHOLD - self.S[-1]))
        # time: t+1
        self.S.append(self.heaviside(self.U[-1]))
        return self.S[-1]

    def forward(self, X):
        """
        X: tensor of shape (batch, input_size)
        """
        # time: t
        I = self.alpha * self.I + X @ self.w
        U = (self.beta * self.U + I) * (THRESHOLD - self.S)
        # time: t+1
        self.U = U
        self.I = I
        if self.is_output:
            return U

        S = self.heaviside(self.U)
        self.S = tf.stop_gradient(S)
        return S 

    def call(self, X):
        return self.forward(X)

    def plot(self, values, slices = (slice(None), 0, 0)):
        if not self.ploting:
            raise Exception("ploting is not set to True in the constructor")
        legend = []
        for value in values:
            x = None
            legend.append(value)
            if value == "I":
                x = self.I
            elif value == "U":
                x = self.U
            elif value == "S":
                x = self.S
            else:
                raise Exception("Unknow value")
            # Start ploting
            t = np.array(self.t[1:])[slices[0]]
            x = np.array(x[1:])[slices]
            plt.plot(t, x)
            plt.xlabel('time in ms')
            plt.ylabel(value)
            plt.grid(True)

        plt.legend(legend)
        plt.show()
        return
