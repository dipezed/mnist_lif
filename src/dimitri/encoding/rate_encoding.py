from dimitri.utils import *

import tensorflow as tf
import numpy as np
np.random.seed(SEED)

def random_encode_value(input_value, encoding_length=TIME_PER_VALUE, max_rate=MAX_RATE):
    """
    Encode a normalized input value (0 to 1) into a random spike train represented by a NumPy array.

    Parameters:
    - input_value: The input value to be encoded, expected to be in the range [0, 1].
    - encoding_length: The length of the resulting spike train array.
    - max_rate: The maximum firing rate to represent the value 1.

    Returns:
    - A NumPy array representing the randoms spike train of the input value.
    """
    assert 0 <= input_value <= 1, "Input value must be in the range [0, 1]."
    res = np.zeros((encoding_length,))

    p_spike = max_rate * input_value * DT  
    i = 0
    while i < encoding_length: 
        rd = np.random.rand()
        if rd < p_spike:
            res[i] = VALUE_SPIKE
        i += 1
    return res 

def rate_encode(
        array,
        encoding_length=TIME_PER_VALUE,
        max_rate = MAX_RATE,
        is_output_tensor = False,
        dtype = 'float32'):
    """
    Parameters:
    - array: np.ndarray of shape(batch, value)

    Returns:
    - A NumPy array of shape(batch, time, value) in the rate encoding
    """
    current_shape = array.shape
    array = array.flatten()
    res = np.zeros((array.size, encoding_length))
    for i in range(array.size):
        res[i] = random_encode_value(array[i], encoding_length, max_rate)
    res = res.reshape(current_shape + (encoding_length,))
    res = np.transpose(res, [2, 0, 1])
    res = res.astype(dtype)
    if is_output_tensor:
        # we want tensorflow array
        return tf.convert_to_tensor(res)
    return res

