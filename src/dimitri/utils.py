from math import exp
# ============= LIF ======================
DT = 0.001 
THRESHOLD = 1
# --------- weight -----------
MIN_VAL = 0
MAX_VAL = 3/2
INHIBITORY = MIN_VAL + 1/4 * MAX_VAL
# -------- backprop ----------
SURR_GRAD_SCALE = 100
LEARNING_RATE = 0.1
# ---- equation constant -----
TAU_SYN = 5e-3
TAU_MEM = 10e-3
ALPHA = exp(-DT / TAU_SYN)
BETA = exp(-DT / TAU_MEM)
# ============= ENCODING =================
TIME_PER_VALUE = 100 
MAX_RATE = 70
VALUE_SPIKE = 1
# ============= CONFIG ===================
SEED = 1
BATCH_SIZE = 50
EPOCH = 100
VERBOSE = 2
